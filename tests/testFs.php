<?php
require_once __DIR__ . "/../vendor/autoload.php";
use pw\fs\fs;
use pw\fs\dir;
use pw\fs\file;

class testFs extends PHPUnit_Framework_TestCase {
	
	/**
	 * @dataProvider testSanitizeProvider
	 */
	public function testSanitize ($x, $y) {
		$fs = new fs ($x);
		$this->assertEquals ($y, $fs->path());
	}
	
	public function testSanitizeProvider () {
		return [
			["a///b//\\c//d", "a/b/c/d"],
			["./a/./b\\c\d", "a/b/c/d"],
			["./a/./b\\c\d/.", "a/b/c/d"],
			["./z/../a/./b\\c\d/.", "a/b/c/d"],
			["z/../a/./b\\c\d/.", "a/b/c/d"],
			["z/../a/./b\\c\d/..", "a/b/c"],
		];
	}
	
	/**
	 * @dataProvider testIsAbsProvider
	 */
	public function testIsAbs ($x, $y) {
		$fs = new fs ($x);
		$this->assertEquals ($y, $fs->isAbs());
	}
	
	
	public function testIsAbsProvider () {
		return [
			["a///b//\\c//d", false],
			["./a/./b\\c\d", false],
			["/a/b/c/", true],
			["/z/../a/b/c", true]
		];
	}
	
	/**
	 * @dataProvider testAbsProvider
	 */
	public function testAbs ($x, $y) {
		$fs = new fs ($x);
		$this->assertEquals ($y, $fs->abs());
	}
	
	
	public function testAbsProvider () {
		return [
			["a///b//\\c//d", dirname(__DIR__) . "/a/b/c/d"],
			["./a/./b\\c\d", dirname(__DIR__) . "/a/b/c/d"],
			["/a/b/c", "/a/b/c"],
			["/z/../a/b/c", "/a/b/c"]
		];
	}
	
	
	/**
	 * @dataProvider testRelProvider
	 */
	public function testRel ($x, $z, $y) {
		$fs = new fs ($x);
		$this->assertEquals ($y, $fs->rel($z));
	}
	
	
	public function testRelProvider () {
		return [
			[__DIR__ . "/a/b/c/d", __DIR__, "a/b/c/d"],
			["./a/./b\\c\d", null, "a/b/c/d"]
		];
	}
	
	/**
	 * @dataProvider testExistsProvider
	 */
	public function testExists ($x, $y) {
		$fs = new fs ($x);
		$this->assertEquals ($y, $fs->exists());
	}
	
	
	public function testExistsProvider () {
		return [
			["a///b//\\c//d", false],
			[__FILE__, true],
			[__DIR__, true]
		];
	}
	
	/**
	 * @dataProvider testPathinfoProvider
	 */
	public function testPathinfo ($x, $y, $z, $w) {
		$fs = new fs ($x);
		$this->assertEquals ($y, $fs->dirname());
		$this->assertEquals ($z, $fs->filename());
		$this->assertEquals ($w, $fs->ext());
	}
	
	public function testPathinfoProvider () {
		return [
			["afsd/wb/asdf\\wef.a", "afsd/wb/asdf", "wef.a", "a"]
		];
	}
	
	public function testLink () {
		$link = new pw\fs\link (__DIR__ . "/testFs-link");
		if ($link->exists ()) 
			$link->remove ();
			
		$link->create (__FILE__);
		
		
		$this->assertTrue ($link->isLink());
		$this->assertInstanceOf ('pw\fs\link', $link);
		$this->assertEquals (__FILE__, $link->target());
		$link->remove ();
		$this->assertFalse ($link->isLink());
	}
	
	public function testMime () {
		$file = new file (__FILE__);
		$this->assertEquals (file::mime_of_ext("php"), $file->mime());
	}
	
	public function testFileData () {
		$str = '<?php return [1,2,3];';
		$f1 = new file (__DIR__ . "/a.php");
		$f1->contents ($str);
		$f2 = new file (__DIR__ . "/a.txt");
		$f2->contents ($str);
		$this->assertEquals (strlen($str), $f1->size());
		$this->assertEquals (strlen($str), $f2->size());
		$this->assertEquals ([1,2,3], $f1->data());
		$this->assertEquals ($str, $f2->data());
		$f2->truncate();
		$this->assertTrue (empty($f2->contents()));
		$f1->remove();
		$f2->remove();
		$this->assertFalse ($f1->exists());
		$this->assertFalse ($f2->exists());
	}
	
	public function testLines () {
		$str = "1\n\n2\n3\n4\n\n5\n6\n7\n";
		$f = new file (__DIR__ . "/b.txt");
		$f->contents($str);
		$this->assertEquals (10, $f->nlines());
		$this->assertEquals ([1,2,3,4,5,6,7], $f->lines());
		$this->assertEquals ([2,3,4,5,6,7], $f->lines(1));
		$this->assertEquals ([1, 2,3,4,5], $f->lines(-2));
		$this->assertEquals (['', 2,3,4, '', 5,6,7, ''], $f->lines(1, false));
		
		$f->remove();
		$this->assertFalse ($f->exists());
		
	}
	
	public function testDir () {
		$dir1 = new dir(__DIR__ . DIRECTORY_SEPARATOR . "test1");
		$subdir = $dir1->path("a/b/c");
		$this->assertEquals (
			__DIR__ .
			DIRECTORY_SEPARATOR . "test1" .
			DIRECTORY_SEPARATOR . "a" .
			DIRECTORY_SEPARATOR . "b" .
			DIRECTORY_SEPARATOR . "c", $subdir);
		$dir = new dir($subdir);
		$dir->create();
		$this->assertTrue ($dir->exists());
		
		
		$dir2 = new dir(__DIR__ . DIRECTORY_SEPARATOR . "test2");
		
		$files = [
			$dir1->path("b/c/d/a.txt"),
			$dir1->path("e/c.txt"),
			$dir1->path("x/d/c.txt"),
			$dir1->path("x/d/p")
		];
		foreach ($files as $file) {
			$fs = new fs($file);
			$ext = $fs->ext();
			if ($ext) $fs->create();
			else $fs->dir()->create();
		}
		$dir = $dir1->dir();
		$this->assertInstanceOf ('pw\fs\fs', $dir);
		$this->assertInstanceOf ('pw\fs\dir', $dir);
		
		$this->assertEquals ([
			__DIR__ . DIRECTORY_SEPARATOR . "test1/a/b/c",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/a/b",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/a",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/b/c/d/a.txt",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/b/c/d",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/b/c",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/b",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/e/c.txt",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/e",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/x/d/c.txt",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/x/d/p",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/x/d",
			__DIR__ . DIRECTORY_SEPARATOR . "test1/x"
		], $dir->contents(false));
		
		if ($dir2->exists()) $dir2->rremove();
		$this->assertFalse($dir2->exists());
		$dir -> rcopy2 ($dir2);
		$this->assertTrue((new fs($dir2->path("b/c/d/a.txt")))->exists());
		
		$dir1->rremove();
		$dir2->rremove();
		$this->assertFalse($dir1->exists());
		$this->assertFalse($dir2->exists());
	}
	
	
}