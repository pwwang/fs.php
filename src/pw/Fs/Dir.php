<?php
namespace pw\Fs;

class Dir extends Fs {
	
	private $handler;
	
	public function contents ($pwfs = true, $recursive = true) {
		$ret = [];
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir() and $recursive) {
				$r = $fs->dir()->contents();
				$ret = array_merge($ret, $r);
			}
			$ret[] = $fs;
		}
		$this->close();
		if (!$pwfs) {
			foreach ($ret as $i => $r)
				$ret[$i] = $r->path();
		}
		return $ret;
	}
	
	
	public function files ($pwfs = true, $recursive = true) {
		$ret = [];
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir() and $recursive) {
				$r = $fs->files();
				$ret = array_merge($ret, $r);
			}
			if ($fs->isFile())
				$ret[] = $fs;
		}
		$this->close();
		
		if (!$pwfs) {
			foreach ($ret as $i => $r)
				$ret[$i] = $r->path();
		}
		return $ret;
	}
	
	public function dirs ($pwfs = true, $recursive = true) {
		$ret = [];
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir()) {
				if ($recursive) {
					$r = $fs->dirs();
					$ret = array_merge($ret, $r);
				}
				$ret[] = $fs;
			}
		}
		$this->close();
		
		if (!$pwfs) {
			foreach ($ret as $i => $r)
				$ret[$i] = $r->path();
		}
		return $ret;
	}
	
	public function links ($pwfs = true, $recursive = true) {
		$ret = [];
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir() and $recursive) {
				$r = $fs->links();
				$ret = array_merge($ret, $r);
			}
			if ($fs->isLink())
				$ret[] = $fs;
		}
		$this->close();
		
		if (!$pwfs) {
			foreach ($ret as $i => $r)
				$ret[$i] = $r->path();
		}
		return $ret;
	}
	
	
	public function glob ($pattern, $fs = true) {
		$ret = glob ($this->path . DIRECTORY_SEPARATOR . $pattern);
		if ($fs) {
			foreach ($ret as $i => $r) 
				$ret[$i] = new Fs($f);
		}
		return $ret;
	}
	
	public function create ($mode = 0755, $recursive = true) {
		return @mkdir ($this->path, $mode, $recursive);
	}
	
	public function rchmod ($mode) {
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir())
				$fs->dir()->rchmod($mode);
			else
				$fs->chmod ($mode);
		}
		$this->close();
		$this->rchmod ($mode);
	}
	
	public function rchgrp ($group) {
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir())
				$fs->dir()->rchgrp($group);
			else
				$fs->chgrp ($group);
		}
		$this->close();
		$this->chgrp ($group);
	}
	
	public function rchown ($own) {
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir())
				$fs->dir()->rchown($own);
			else
				$fs->chown ($own);
		}
		$this->close();
		$this->chown ($own);
	}
	
	public function rcopy2 ($new) {
		if (!($new instanceof \pw\Fs\Fs))
			$new = new Dir ($new);
		
		$new->create();
		
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue; // ., ..
			if ($fs->isDir())
				$fs->dir()->rcopy2($new->path($fs->filename()));
			else
				$fs->copy2 ($new->path($fs->filename()));
		}
		$this->close();
	}
	
	public function open () {
		$this->handler = opendir ($this->path);
		if (!$this->handler)
			throw new \Exception ("Failed to open dir: " . $this->path());
		return $this->handler;
	}
	
	public function close () {
		if ($this->handler)
			closedir($this->handler);
	}
	
	public function read ($fs = true) {
		$ret = readdir ($this->handler);
		if ($ret == '.' or $ret == '..') return true;
		if ($ret === false) return false;
		return $fs
			? (new Fs ($this->path($ret)))
			: $ret;
	}
	
	public function rewind () {
		return rewinddir ($this->handler);
	}
	
	public function rremove () {
		$this->truncate ();
		rmdir ($this->path);
	}
	
	public function truncate () {
		$this->open ();
		while (($fs = $this->read()) !== false) {
			if ($fs === true) continue;
			if ($fs->isDir())
				$fs->dir()->rremove();
			else
				$fs->remove();
		}
		$this->close();
	}
	
}
