<?php
namespace pw\Fs;

class link extends Fs {
	
	public function create ($target, $recursive = true) {
		if ($recursive) {
			$dir = $this->parent();
			if (!$dir->exists())
				$dir->create();
		}
		return @symlink ($target, $this->path);
	}
	
	public function target () {
		return readlink ($this->path);
	}
	
	public function file () {
		return new File ($this->target());
	}
}
