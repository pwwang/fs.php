<?php
namespace pw\Fs;
use pw\Sysinfo\Sysinfo;

class Fs {
	
	const TYPE_FILE = 0;
	const TYPE_DIR  = 1;
	const TYPE_LINK = 2;
	const TYPE_UNKNOWN =3;
	
	protected $path;
	protected $is_abs;
	
	public function __construct ($path) {
		$this->path   = self::sanitize($path);
		$this->is_abs = self::is_absolute($this->path);
	}
	
	public function type () {
		if (is_link($this->path))
			return self::TYPE_LINK;
		if (is_file($this->path))
			return self::TYPE_FILE;
		if (is_dir ($this->path))
			return self::TYPE_DIR;
		return self::TYPE_UNKNOWN;
	}
	
	public function __call ($name, $arguments) {
		$type = $this->type();
		switch ($name) {
			case 'is_file':
			case 'isFile':
				return $type == self::TYPE_FILE;
			case 'is_dir':
			case 'isDir':
			case 'is_folder':
			case 'isFolder':
				return $type == self::TYPE_DIR;
			case 'is_link':
			case 'isLink':
				return $type == self::TYPE_LINK;
			case 'file':
				return new File($this->path);
			case 'dir':
				return new Dir ($this->path);
			case 'link':
				return new Link($this->path);
			case 'is_abs':
			case 'isAbs':
				return $this->is_abs;
			case 'dirname':
				return dirname ($this->path);
			case 'ext':
			case 'extension':
				return pathinfo($this->path, PATHINFO_EXTENSION);
			case 'parent':
				return new Dir (dirname($this->path));
			default:
				throw new \InvalidArgumentException("Unknown method $name for class " . __CLASS__);
		}
	}
	
	public function __toString () {
		return $this->path;
	}
	
	public function filename ($withdir = false) {
		$ret = pathinfo ($this->path, PATHINFO_FILENAME);
		if ($withdir)
			$ret = $this->dirname() . DIRECTORY_SEPARATOR . $ret;
		return $ret;
	}
	
	public function basename ($withdir = false) {
		$ret = pathinfo ($this->path, PATHINFO_BASENAME);
		if ($withdir)
			$ret = $this->dirname() . DIRECTORY_SEPARATOR . $ret;
		return $ret;
	}
	
	public function path ($suffix = null, $fs = false) {
		if (!is_null($suffix))
			$path = $this->path . DIRECTORY_SEPARATOR . $suffix;
        else $path = $this->path;
        
        return $fs ? (new Fs ($path)) : $path;
	}
	
	public function exists () {
		$type = $this->type();
		return $type != self::TYPE_UNKNOWN;
	}
	
	public function abs () {
		if ($this->is_abs) return $this->path;
		
		$path  = self::sanitize(getcwd() . DIRECTORY_SEPARATOR . $this->path);
		$parts = explode (DIRECTORY_SEPARATOR, $path);
		
		for ($i = sizeof($parts); $i >= 1; $i--) {
			$cparts = array_slice ($parts, 0, $i);
			$rests  = array_slice ($parts, $i);
			$cpath  = implode(DIRECTORY_SEPARATOR, $cparts);
			$real   = realpath ($cpath);
			if ($real !== false) {
				return $cpath . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $rests);
			}
		}
		return false;
	}
	
	public function rel ($base = null) {
		if (!$this->isAbs()) return $this->path;
		if (is_null($base)) $base = self::cwd();
		if (!($base instanceof self))
			$base = new self ($base);
		
		return substr($this->path, strlen ($base->path())+1);
	}
	
	public function is ($path) {
		if (!($path instanceof self))
			$path = new self($path);
		return $this->abs() == $path->abs();
	}
	
	public function remove () {
		return unlink ($this->path);
	}
	
	public function touch ($time = null) {
		if (is_null($time)) $time = time();
		return touch ($this->path, $time);
	}
	
	public function create ($mode = 0755, $recursive = true) {
		if ($recursive) {
			$dir = $this->parent();
			if (!$dir->exists())
				$dir->create();
		}
		$ret = $this->touch ();
		$ret &= $this->chmod ($mode);
		return $ret;
	}
	
	public function mode ($mode = null) {
		if (is_null($mode)) {
			$mode = fileperms ($this->path);
			return sprintf("%04d", decoct($mode & 0777));
		} 
		return chmod ($this->path, $mode);
	}
	
	public function chmod ($mode) {
		return $this->mode ($mode);
	}
	
	public function group ($group = null) {
		if (is_null($group)) {
			$group = filegroup ($this->path);
			if (function_exists("posix_getgrgid")) {
				$group = posix_getgrgid($group);
				return $group["name"];
			}
			return "";
		}
		return chgrp ($this->path, $group);
	}
	
	public function chgrp ($group) {
		return $this->group($group);
	}
	
	public function owner ($own = null) {
		if (is_null($own)) {
			$owner = fileowner ($this->path);
			if (function_exists("posix_getpwuid")) {
				$owner = posix_getpwuid($owner);
				return $owner['name'];
			}
			return "";
		}
		return chown ($this->path, $own);
	}
	
	public function chown ($own) {
		return $this->owner ($own);
	}
	
	public function move2 ($new) {
		return rename ($this->path, $new);
	}
	
	public function copy2 ($new) {
		return copy ($this->path, $new);
	}
	
	static public function cwd ($suffix = null) {
    $suffix = is_null($suffix) ? "" : DIRECTORY_SEPARATOR . $suffix;
		return new self(getcwd() . $suffix);
	}
	
	static public function sanitize ($path) {
		
		$qs = preg_quote (DIRECTORY_SEPARATOR);
		$lastlen = strlen ($path);
		// check http://stackoverflow.com/questions/21421569/sanitize-file-path-in-php-without-realpath
		while (true) {
			$path = preg_replace (
				[
					"#[\\\/]+#",
					"#$qs\.$qs#",
					"#^\.$qs#",
					"#$qs\.$#",
					"#(^|$qs)[^$qs]$qs\.\.$qs#",
					"#(^|$qs)[^$qs]$qs\.\.$#",
				],
				[
					DIRECTORY_SEPARATOR,
					DIRECTORY_SEPARATOR,
					"",
					"",
					"$1",
					""
				],
				$path
			); 
			
			$len = strlen ($path);
			if ($len == $lastlen) break;
			$lastlen = $len;
		}
		return $path;
	}
	
	static public function is_absolute ($path) {
		list ($first) = explode(DIRECTORY_SEPARATOR, $path);
		if (empty($first)) return true;
		
		if (Sysinfo::os() == Sysinfo::OS_WIN) 
			return preg_match ("/^[A-Z]:$/", $first);
		return false;
	}
}

